# Logic Challenge - Balik Kata

### NOTES

1. Clone repository
2. Masuk ke repository yang telah di clone
3. git checkout -b "nama"
4. Edit di file index.js
5. git add .
6. git commit -m "pesan"
7. git push origin "nama"

### RESTRICTION

- Tidak boleh menggunakan built-in function apapun

### HINTS

---

## Objectives

- Mengerti logika `looping`
- Dapat mengakses indeks dari sebuah String dari indeks awal sampai akhir ataupun sebaliknya

## Directions

Diberikan sebuah variabel `kata` bertipe String. Buatlah program yang dapat membalikkan `kata` tersebut tanpa menggunakan built in function apapun dari javascript. Kerjakan dengan menggunakan looping!

```
Contoh: let kata = 'Javascript'

maka output yang dihasilkan adalah 'tpircsavaJ'
```
